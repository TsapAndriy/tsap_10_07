﻿<%@ Page Language="C#" AutoEventWireup="true" Theme="DefaultTheme" CodeBehind="Default.aspx.cs" Inherits="BeStyle.Admin.WebUI.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Default</title>
</head>
<body>
    <form id="form1" runat="server">
    <header>
        <asp:Button runat="server" Text="Sign Out" ID="btnSignOut" OnClick="btnSignOut_OnClick"/>
        <asp:Label ID="lblUserName" runat="server" Text="User name: "></asp:Label>
    </header>
    <div>
        <asp:Button ID="btnMasterPage" runat="server" Text="Master Page" Visible="False" OnClick="btnMasterPage_OnClick"/>
        <asp:Button runat="server" ID="btnModeratorPage" Text="Moderator Page" Visible="False"/>
        <asp:Button runat="server" ID="btnEditorPage" Text="Editor Page" Visible="False"/>
    </div>
    </form>
</body>
</html>
