﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using BeStyle.Admin.WebUI.Code.Bll.Code;
using BeStyle.Admin.WebUI.Code.Bll.Entities;
using BeStyle.Admin.WebUI.Code.Roles;
using BeStyle.Entities;
using BeStyle.Repositories.Sql;


namespace BeStyle.Admin.WebUI.Pages.Master
{
    public partial class Admins : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnInsert_OnClick(object sender, EventArgs e)
        {
            var userBll = new UserBll();
            userBll.FirstName = ((TextBox)gvUsers.FooterRow.FindControl("tbxFirstName")).Text;
            userBll.LastName = ((TextBox)gvUsers.FooterRow.FindControl("tbxLastName")).Text;
            userBll.Email = ((TextBox)gvUsers.FooterRow.FindControl("tbxEmail")).Text;
            userBll.Phone = ((TextBox)gvUsers.FooterRow.FindControl("tbxPhone")).Text;
            userBll.Login = ((TextBox)gvUsers.FooterRow.FindControl("tbxLogin")).Text;
            userBll.Password = ((TextBox)gvUsers.FooterRow.FindControl("tbxPassword")).Text;
            userBll.IsMaster = ((CheckBox)gvUsers.FooterRow.FindControl("cbxMaster")).Checked;
            userBll.IsModerator = ((CheckBox)gvUsers.FooterRow.FindControl("cbxModerator")).Checked;
            userBll.IsEditor = ((CheckBox)gvUsers.FooterRow.FindControl("cbxEditor")).Checked;
            userBll.IsUser = ((CheckBox)gvUsers.FooterRow.FindControl("cbxUser")).Checked;
            if (!BllUserManager.InsertUserInfo(userBll))
            {
                // show error
            }
            else
            {
                gvUsers.DataBind();
            }
        }

        protected void btnBack_OnClick(object sender, ImageClickEventArgs e)
        {
            
            Response.Redirect("~/Default.aspx");
        }
    }
}