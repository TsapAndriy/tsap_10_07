﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Configuration;
using BeStyle.Repositories.Sql;
using System.IO;
using System.Web.UI.WebControls;
using BeStyle.Entities;
using System.Drawing.Imaging;
using BeStyle.Admin.WebUI.Code;


namespace BeStyle.Admin.WebUI.Pages.Master
{
    public partial class Products : System.Web.UI.Page
    {
        private string connectionString = WebConfigurationManager.ConnectionStrings["BeStyleDBConnectionString"].ConnectionString;
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void obsProducts_ObjectCreating(object sender, ObjectDataSourceEventArgs e)
        {
            string connString = WebConfigurationManager.ConnectionStrings["BeStyleDBConnectionString"].ConnectionString;
            e.ObjectInstance = new ProductRepository(connString);
        }


        public string GetImage(object img)
        {
            {
                return "data:image/png;base64," + Convert.ToBase64String((byte[])img);
            }
        }

        public string GetCategory(int id)
        {
            string connString = WebConfigurationManager.ConnectionStrings["BeStyleDBConnectionString"].ConnectionString;
            CategoryRepository category = new CategoryRepository(connString);
            return category.GetCategoryValueById(id);
        }

        protected void btnInsert_Click(object sender, EventArgs e)
        {
            ProductEntity product = new ProductEntity();
            product.Name = ((TextBox)gvProducts.FooterRow.FindControl("tbxName")).Text;
            product.Price = Convert.ToDecimal(((TextBox)gvProducts.FooterRow.FindControl("tbxPrice")).Text);
            product.CategoryId = Convert.ToInt32(((DropDownList)gvProducts.FooterRow.FindControl("ddlCategory")).SelectedValue);
            FileUpload flup = (FileUpload)gvProducts.FooterRow.FindControl("flupImage");
            if (flup.HasFile)
            {
                byte[] productImage = flup.FileBytes;
                product.Picture = productImage;
            }
            ProductRepository prRep = new ProductRepository(connectionString);
            prRep.CreateProduct(product);
            gvProducts.DataBind();
        }                        
    }
}

