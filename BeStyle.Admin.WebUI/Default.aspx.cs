﻿using System;
using System.Diagnostics;
using System.Web.Security;
using BeStyle.Admin.WebUI.Code.Roles;

namespace BeStyle.Admin.WebUI
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (User.IsInRole(AdminRoles.Master))
            {
                btnMasterPage.Visible = true;
                btnModeratorPage.Visible = true;
                btnEditorPage.Visible = true;
            }
            else if (User.IsInRole(AdminRoles.Moderator))
            {
                btnModeratorPage.Visible = true;
                btnEditorPage.Visible = true;
            }
            else if (User.IsInRole(AdminRoles.Editor))
            {
                btnEditorPage.Visible = true;
            }
            #region Output info

            lblUserName.Text = "User Name: " + User.Identity.Name + ".| Your roles: ";
            
            if (User.IsInRole(AdminRoles.Master))
            {
                lblUserName.Text += AdminRoles.Master + "  ";
            }
            if (User.IsInRole(AdminRoles.Moderator))
            {
                lblUserName.Text += AdminRoles.Moderator + "  ";
            }
            if (User.IsInRole(AdminRoles.Editor))
            {
                lblUserName.Text += AdminRoles.Editor + "  ";
            }
            if (User.IsInRole(AdminRoles.User))
            {
                lblUserName.Text += AdminRoles.User + "  ";
            }

            #endregion

        }

        protected void btnSignOut_OnClick(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            FormsAuthentication.RedirectToLoginPage();
        }

        protected void btnMasterPage_OnClick(object sender, EventArgs e)
        {
            Response.Redirect("Pages/Master/Admins.aspx");
        }
    }
}