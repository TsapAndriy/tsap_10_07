﻿namespace BeStyle.Admin.WebUI.Code.Bll.Entities
{
    public class UserBll
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public bool IsMaster { get; set; }
        public bool IsModerator { get; set; }
        public bool IsEditor { get; set; }
        public bool IsUser { get; set; }
    }
}