﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using BeStyle.Admin.WebUI.Code.Bll.Entities;
using BeStyle.Admin.WebUI.Code.Roles;
using BeStyle.Entities;
using BeStyle.Repositories.Sql;
using System.Web.Configuration;

namespace BeStyle.Admin.WebUI.Code.Bll.Code
{
    public static class BllUserManager
    {
        private static readonly string ConnectionString = WebConfigurationManager.ConnectionStrings["BeStyleDBConnectionString"].ConnectionString;
        public static List<UserBll> GetAllUsers()
        {
            var usersBll = new List<UserBll>();
            var db = new UserRepository(ConnectionString);
            var users = db.GetUsersAll();
            foreach (var user in users)
            {
                var userBll = new UserBll();
                userBll.Login = user.Login;
                userBll.Email = user.Email;
                userBll.Password = user.Password;
                userBll.FirstName = user.FirstName;
                userBll.LastName = user.LastName;
                userBll.Id = user.Id;
                userBll.Phone = user.Phone;
                userBll = CheckRoles(user, userBll);
                usersBll.Add(userBll);
            }
            return usersBll;
        }

        public static void UpdateUserInfo(int Id, string FirstName, string LastName, string Email, string Phone, string Login,
            string Password, bool IsMaster, bool IsModerator, bool IsEditor, bool IsUser)
        {
            var userBll = new UserBll()
            {
                Id = Id,
                FirstName = FirstName ?? string.Empty,
                LastName = LastName ?? string.Empty,
                Email = Email,
                Phone = Phone ?? string.Empty,
                Login = Login,
                Password = Password,
                IsMaster = IsMaster,
                IsModerator = IsModerator,
                IsEditor = IsEditor,
                IsUser = IsUser,                
            };
            UpdateUserInfo(userBll);
        }

        public static void UpdateUserInfo(UserBll userBll)
        {
            var user = GetUserInfo(userBll);
            var db = new UserRepository(ConnectionString);
            db.UpdateUserInfo(user);
        }

        public static bool InsertUserInfo(UserBll userBll)
        {
            var db = new UserRepository(ConnectionString);
            var user = GetUserInfo(userBll);
            
            return db.CreateUser(user);
        }

        public static void DeleteUserInfo(int Id)
        {
            var db = new UserRepository(ConnectionString);
            db.DeleteUser(Id);
        }

        private static UserEntity GetUserInfo(UserBll userBll)
        {
            var user = new UserEntity()
            {
                Login = userBll.Login,
                Password = userBll.Password,
                FirstName = userBll.FirstName,
                LastName = userBll.LastName,
                Email = userBll.Email,
                Id = userBll.Id,
                Phone = userBll.Phone ?? ""
            };
            if (userBll.IsMaster)
            {
                user.Roles.Add(AdminRoles.Master);
            }
            if (userBll.IsModerator)
            {
                user.Roles.Add(AdminRoles.Moderator);
            }
            if (userBll.IsEditor)
            {
                user.Roles.Add(AdminRoles.Editor);
            }
            if (userBll.IsUser)
            {
                user.Roles.Add(AdminRoles.User);
            }

            return user;
        }

        private static UserBll CheckRoles(UserEntity from, UserBll to)
        {
            if (from.Roles == null)
            {
                return to;
            }
            foreach (var role in from.Roles)
            {
                if (role == AdminRoles.Master)
                {
                    to.IsMaster = true;
                }
                else if (role == AdminRoles.Moderator)
                {
                    to.IsModerator = true;
                }
                else if (role == AdminRoles.Editor)
                {
                    to.IsEditor = true;
                }
                else if (role == AdminRoles.User)
                {
                    to.IsUser = true;
                }
            }
            return to;
        }
    }
}