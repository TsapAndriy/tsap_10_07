﻿using System.Collections.Generic;
using BeStyle.Entities;

namespace BeStyle.Repositories.Abstract
{
    public interface IUserRepository
    {
        List<UserEntity> GetUsersAll();       
        List<string> GetAllRolesValueForUser(int userId);
        void UpdateUserInfo(UserEntity user);
        void DeleteUser(int userId);
        bool CreateUser(UserEntity user);
        UserEntity GetUserByLogin(string login);
        UserEntity GetUserByEmail(string email);
    }
}
