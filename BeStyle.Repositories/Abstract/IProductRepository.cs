﻿using System.Collections.Generic;
using BeStyle.Entities;
using System.Data;

namespace BeStyle.Repositories.Abstract
{
    public interface IProductRepository
    {
        List<ProductEntity> GetProductsAll();
        ProductEntity GetProductById(int id);
        void UpdateProduct(ProductEntity product);
        void DeleteProduct(int productId);
        void CreateProduct(ProductEntity product);        
        CategoryEntity GetCategoryForProductById(int id);
    }
}
