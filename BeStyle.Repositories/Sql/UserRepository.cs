﻿using BeStyle.Repositories.Abstract;
using BeStyle.Entities;
using System.Collections.Generic;
using System.Data.SqlClient;
using System;
using System.Data;
using System.Text;


namespace BeStyle.Repositories.Sql
{
    public class UserRepository : IUserRepository
    {
        #region Queryies
        private const string GET_ALL_QUERY = "SELECT * FROM tblUser";
        private const string GET_ALL_ROLES_FOR_USER="SELECT * FROM tblUserRole WHERE UserId=@Id";
        #endregion
        #region Fields

        private readonly string _connectionString;
        private string _commandString;

        #endregion

        #region IUserRepository

        public UserRepository(string connectionString)
        {
            this._connectionString = connectionString;
        }

        public List<UserEntity> GetUsersAll()
        {
            List<UserEntity> users = new List<UserEntity>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();                
                using (SqlCommand cmd = new SqlCommand(GET_ALL_QUERY, connection))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            UserEntity user = ReadUser(reader);
                            users.Add(user);
                        }
                        return users;
                    }
                }
            }  
        }

        public List<string> GetAllRolesForUser(int userId)
        {
            var roles = new List<string>();
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();                
                using (var command = new SqlCommand(GET_ALL_ROLES_FOR_USER, connection))
                {
                    command.Parameters.AddWithValue("Id", userId);
                    using (var reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string role = ReadRole(reader);
                            roles.Add(role);
                        }
                        return roles.Count == 0 ? null : roles;
                    }
                }
            }
        }    
        

        public List<string> GetAllRolesValueForUser(int userId)
        {
            List<string> roles = new List<string>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                _commandString = String.Format(@"SELECT role.Role
                                               FROM tblUserRole roleid
                                               INNER JOIN tblRole role
                                               ON roleid.RoleId=role.id 
                                               WHERE roleid.UserId={0}", userId);
                using (SqlCommand cmd = new SqlCommand(_commandString, connection))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string role = ReadRole(reader);
                            roles.Add(role);
                        }                        
                    }
                }
            }
            return roles.Count == 0 ? null : roles;

        }

        public void UpdateUserInfo(UserEntity user)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                const string query =
                    @"UPDATE tblUser SET FirstName=@FirstName, LastName=@LastName, Email=@Email, Phone=@Phone, Login=@Login, Password=@Password WHERE Id=@Id";
                using (var cmd = new SqlCommand(query, connection))
                {
                    cmd.Parameters.AddWithValue("@FirstName", user.FirstName);
                    cmd.Parameters.AddWithValue("@LastName", user.LastName);
                    cmd.Parameters.AddWithValue("@Email", user.Email);
                    cmd.Parameters.AddWithValue("@Phone", user.Phone);
                    cmd.Parameters.AddWithValue("@Login", user.Login);
                    cmd.Parameters.AddWithValue("@Password", user.Password);
                    cmd.Parameters.AddWithValue("@Id", user.Id);
                    cmd.ExecuteNonQuery();
                    UpdateRoles(user.Id, SetRoleId(user.Roles));
                }
            }
        }

        public void DeleteUser(int userId)
        {
            DeleteRolesForUser(userId);
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                const string query = @"DELETE FROM tblUser WHERE Id=@UserId";
                using (var cmd = new SqlCommand(query, connection))
                {
                    cmd.Parameters.AddWithValue("@UserId", userId);
                    cmd.ExecuteNonQuery();
                }
            }

        }

        public bool CreateUser(UserEntity user)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                _commandString = string.Format("SELECT Id FROM tblUser WHERE Login='{0}'", user.Login);
                using (var cmd = new SqlCommand(_commandString, connection))
                {
                    var ret = cmd.ExecuteScalar();
                    if (ret != null)
                    {
                        return false;
                    }
                    _commandString = string.Format("SELECT Id FROM tblUser WHERE Email='{0}'", user.Email);
                    cmd.CommandText = _commandString;
                    ret = cmd.ExecuteScalar();
                    if (ret != null)
                    {
                        return false; ;
                    }
                }
                var query = string.Format("INSERT INTO tblUser (FirstName, LastName, Email, Phone, Login, Password) VALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}');\n\rSELECT SCOPE_IDENTITY();",
                                            user.FirstName, user.LastName, user.Email,
                                            user.Phone, user.Login, user.Password);
                using (var cmd = new SqlCommand(query, connection))
                {
                    var id = (int)(decimal)cmd.ExecuteScalar();
                    CreateRoles(id, SetRoleId(user.Roles));
                }            
            }
            return true;
        }

      
        public UserEntity GetUserByLogin(string login)
        {
            using (SqlConnection connection=new SqlConnection(_connectionString))
            {
                connection.Open();
                _commandString = String.Format(@"SELECT * FROM tblUser WHERE Login='{0}' COLLATE SQL_Latin1_General_CP1_CS_AS", login);
                using (SqlCommand cmd=new SqlCommand(_commandString,connection))
                {                    
                    using(SqlDataReader reader=cmd.ExecuteReader())
                    if (reader.Read())
                    {
                        return ReadUser(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }

        public UserEntity GetUserByEmail(string email)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var command = String.Format(@"SELECT * FROM tblUser WHERE Email='{0}'", email);
                using (var cmd = new SqlCommand(command, connection))
                {
                    using (var reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return ReadUser(reader);
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }
        

        #endregion

        #region Helpers

        private void DeleteRolesForUser(int userId)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                _commandString = @"DELETE FROM tblUserRole WHERE UserId=@UserId";
                using (var cmd = new SqlCommand(_commandString, connection))
                {
                    cmd.Parameters.AddWithValue("@UserId", userId);
                    cmd.ExecuteNonQuery();
                }
            }
        }

        private void UpdateRoles(int userId, IEnumerable<int> roles)
        {
            DeleteRolesForUser(userId);
            CreateRoles(userId, roles);
        }

        private void CreateRoles(int userId, IEnumerable<int> roles)
        {
            using (var connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                var insertQuery = new StringBuilder();
                foreach (var roleId in roles)
                {
                    insertQuery.AppendLine(string.Format("INSERT INTO tblUserRole (UserId, RoleId) VALUES ({0}, {1});", userId, roleId));
                }
                using (var cmd = new SqlCommand(insertQuery.ToString(), connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }

        }

        private UserEntity ReadUser(SqlDataReader reader)
        {
            UserEntity user = new UserEntity();
            user.Id = (int)reader["Id"];
            user.FirstName = (string)reader["FirstName"];
            user.LastName = (string)reader["LastName"];
            user.Login = (string)reader["Login"];
            user.Password = (string)reader["Password"];
            user.Email = (string)reader["Email"];
            user.Phone = reader["Phone"].ToString();
            user.Roles = GetAllRolesValueForUser((int)reader["Id"]);
            return user;
       }
        private string ReadRole(SqlDataReader reader)
        {
            return reader["Role"].ToString();
        }
        private List<int> SetRoleId(List<string> roles)
        {
            List<int> roleId = new List<int>();
            foreach (var i in roles)
            {
                switch (i)
                {
                    case "Master": roleId.Add(1); break;
                    case "Moderator": roleId.Add(2); break;
                    case "Editor": roleId.Add(3); break;
                    case "User": roleId.Add(4); break;
                }
            }
            return roleId;
        }

        #endregion
    }
}