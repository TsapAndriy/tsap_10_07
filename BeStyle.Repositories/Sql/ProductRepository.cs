﻿using BeStyle.Entities;
using BeStyle.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;

namespace BeStyle.Repositories.Sql
{
    public class ProductRepository : IProductRepository
    {       
        #region Queryies
        private const string GET_ALL_QUERY = "SELECT * FROM tblProduct";
        private const string DELETE_BY_ID_QUERY = "DELETE FROM tblProduct WHERE Id=@ProductId";
        private const string INSERT_QUERY = "INSERT INTO tblProduct(Name,CategoryId,Price,Picture) VALUES(@Name, @CategoryId, @Price, @Picture)";
        public const string UPDATE_QUERY = "UPDATE tblProduct SET Name=@Name, CategoryId=@CategoryId, Price=@Price, Picture=@Picture WHERE Id=@Id";
        public const string GET_BY_ID_QUERY = "SELECT * FROM tblProduct WHERE Id=@Id";
        public const string GET_CATEGORY_FOR_PRODUCT_QUERY = @"SELECT category.Category 
                                                             FROM tblProduct product
                                                             INNER JOIN tblCategory category
                                                             WHERE product.Id=@Id";

        #endregion
        #region Fields
        private readonly string _connectionString;
        #endregion
        #region Constructors
        public ProductRepository(string connectionString)
        {
            this._connectionString = connectionString;
        }
        #endregion
        #region IProductRepository
        public List<ProductEntity> GetProductsAll()
        {
            List<ProductEntity> products = new List<ProductEntity>();
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_ALL_QUERY, connection))
                {
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ProductEntity product = ReadProduct(reader);
                            products.Add(product);
                        }
                        return products;
                    }
                }
            }
        }

        public void UpdateProduct(ProductEntity product)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(UPDATE_QUERY, connection))
                {
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = product.Id;
                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = product.Name;
                    cmd.Parameters.Add("@CategoryId", SqlDbType.Int).Value = product.CategoryId;
                    cmd.Parameters.Add("@Price", SqlDbType.Decimal).Value = product.Price;
                    cmd.Parameters.Add("@Picture", SqlDbType.VarBinary).Value = product.Picture;
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public void DeleteProduct(int productId)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (var cmd = new SqlCommand(DELETE_BY_ID_QUERY, connection))
                {
                    cmd.Parameters.AddWithValue("ProductId", productId);
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public void CreateProduct(ProductEntity product)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(INSERT_QUERY, connection))
                {
                    cmd.Parameters.Add("@Name", SqlDbType.NVarChar).Value = product.Name;
                    cmd.Parameters.Add("@CategoryId", SqlDbType.Int).Value = product.CategoryId;
                    cmd.Parameters.Add("@Price", SqlDbType.Decimal).Value = product.Price;
                    cmd.Parameters.Add("@Picture", SqlDbType.VarBinary).Value = product.Picture;
                    cmd.ExecuteNonQuery();
                }
            }
        }
        public ProductEntity GetProductById(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();

                using (SqlCommand cmd = new SqlCommand(GET_BY_ID_QUERY, connection))
                {
                    cmd.Parameters.Add("@Id", SqlDbType.Int).Value = id;
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            return ReadProduct(reader);
                        }
                        else
                        {
                            return null;
                        }
                    }
                }
            }
        }

        public CategoryEntity GetCategoryForProductById(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();
                using (SqlCommand cmd = new SqlCommand(GET_CATEGORY_FOR_PRODUCT_QUERY, connection))
                {
                    cmd.Parameters.AddWithValue("Id", id);
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        return ReadCategory(reader);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
        }
        #endregion
        #region Helpers
        private ProductEntity ReadProduct(SqlDataReader reader)
        {
            ProductEntity product = new ProductEntity();
            product.Id = (int)reader["Id"];
            product.Name = (string)reader["Name"];
            product.Price = (decimal)reader["Price"];
            product.Picture = (byte[])reader["Picture"];
            product.CategoryId = (int)reader["CategoryId"];
            return product;
        }

        private CategoryEntity ReadCategory(SqlDataReader reader)
        {
            CategoryEntity category = new CategoryEntity();
            category.Id = (int)reader["Id"];
            category.Category = (string)reader["Category"];
            return category;
        }
        #endregion
    }
}
