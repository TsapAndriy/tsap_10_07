﻿using BeStyle.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeStyle.Repositories.Sql
{
    public class CategoryRepository
    {
        private readonly string _connectionString;
        private const string GET_CATEGORY_BY_ID = "SELECT * FROM tblCategory WHERE Id=@Id";
        public CategoryRepository(string connectionString)
        {
            this._connectionString = connectionString;
        }

        public string GetCategoryValueById(int id)
        {
            using (SqlConnection connection = new SqlConnection(_connectionString))
            {
                connection.Open();                
                using (SqlCommand cmd = new SqlCommand(GET_CATEGORY_BY_ID, connection))
                {
                    cmd.Parameters.AddWithValue("Id", id);
                    using (SqlDataReader reader = cmd.ExecuteReader())
                    {
                        reader.Read();
                        return (string)reader["Category"];
                    }
                }
            }
        }
     
        private CategoryEntity ReadCategory(SqlDataReader reader)
        {
            CategoryEntity category = new CategoryEntity();
            category.Id = (int)reader["Id"];
            category.Category = (string)reader["Category"];
            return category;
        }
    }
}
