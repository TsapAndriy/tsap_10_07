﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BeStyle.Entities
{
    public class UserEntity
    {
        public UserEntity()
        {
            Roles = new List<string>();
        }

        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public List<string> Roles { get; set; }

        public string CommaSeparatedRoles
        {
            get
            {
                if (Roles == null || Roles.Count == 0)
                {
                    return string.Empty;
                }
                else
                {
                    var retValue = new StringBuilder();
                    foreach (var role in Roles)
                    {
                        retValue.Append(role);
                        retValue.Append(",");
                    }

                    return retValue.ToString(0, retValue.Length - 1);
                }
            }
        }
    }
}
