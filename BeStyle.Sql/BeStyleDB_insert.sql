USE BeStyleDB;

GO

SET IDENTITY_INSERT tblUser ON;
INSERT INTO tblUser(Id, FirstName, LastName, Email, Phone, Login, Password)
VALUES
	(1,'Vitaly','Mogola','vitalijmogola@gmail.com','+380673357055', 'Vitaly', 'Vitaly'),
	(2,'Andriy','Tsap','lembergtsap@gmail.com','', 'Andriy', 'Andriy'),
	(3,'Volodymyr','Teodorovich','vladukteo@gmail.com','','Volodia','Volodia');	
SET IDENTITY_INSERT tblUser OFF;

SET IDENTITY_INSERT tblRole ON;
INSERT INTO tblRole(Id, Role)
VALUES 
		(1, 'Master'),
		(2, 'Moderator'),
		(3, 'Editor'),
		(4, 'User');
SET IDENTITY_INSERT tblRole OFF;

SET IDENTITY_INSERT tblUserRole ON;
INSERT INTO tblUserRole(Id, UserId, RoleId)
VALUES
		(1,1,1),
		(2,2,2),
		(3,3,3);
SET IDENTITY_INSERT tblUserRole OFF;

SET IDENTITY_INSERT tblCategory ON
INSERT INTO tblCategory(Id, Category)
VALUES
	(1, 'Shirt'),
	(2, 'Blouse'),
	(3,'Jacket');
SET IDENTITY_INSERT tblCategory OFF;

SET IDENTITY_INSERT tblSize ON;
INSERT INTO tblSize(Id,Size)
VALUES
	(1,'S'),
	(2,'M'),
	(3,'L'),
	(4,'XL'),
	(5,'XXl');
SET IDENTITY_INSERT tblSize OFF;

SET IDENTITY_INSERT tblProduct ON;
INSERT INTO tblProduct(Id, Name, Price, CategoryId, Picture)
SELECT 1, 'T-Shirt Adidas', 215, 1, BulkColumn 
FROM Openrowset( Bulk 'C:\FOTO\1.png', Single_Blob) as ProductPicture;
SET IDENTITY_INSERT tblProduct OFF;	
	
